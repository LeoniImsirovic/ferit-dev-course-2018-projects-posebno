<?php

class Connection
{
    const DB_NAME = 'projekti';
    const HOSTNAME = 'localhost'; // 127.0.0.1
    const USERNAME = 'root';
    const PASSWORD = '';
    /**
     * Connection constructor.
     */
    public function __construct()
    {
        try{
            $conn = new PDO(
                "mysql:host=".
                self::HOSTNAME .
                ";dbname=" . self::DB_NAME,
                self::USERNAME,
                self::PASSWORD
            );
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            echo "konekcija uspješna";

            return $conn;
        } catch (PDOException $e){
            die("Connection failed: " . $e->getMessage());
        }
    }
}